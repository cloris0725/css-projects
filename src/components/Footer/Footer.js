import React, {Component} from "react";
import "../component.css"


class Footer extends Component{
    render() {
        return(
            <div className="footer">
                <div className="footer_section">Contact Us</div>
                <div className="footer_section">News & Resources</div>
                <div className="footer_section">Posts</div>
            </div>
        )
    }
}


export default Footer