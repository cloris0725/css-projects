import React, {Component} from "react";
import "../component.css"
import Logo from "../../pizza-image/img/logo.png"


class Header extends Component{
    render() {
        return (
            <div >
                <div className="header">
                    <div className="outlineEle"><img
                        src={Logo} alt=""/></div>
                    <div className="outlineEle"><h1>Menu</h1></div>

                </div>

            </div>
        );
    }
}

export default Header