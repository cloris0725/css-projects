import React from 'react';
import Header from "./components/Header/Header";
import OrderSection from "./components/order-section/OrderSection";
import Footer from "./components/Footer/Footer";



// import './App.css';

function App() {
  return (
      <div className="App">
        <Header></Header>
          <OrderSection></OrderSection>
          <Footer></Footer>

      </div>
  );
}

export default App;
